using UnityEngine;
public class CapsuleScaling : MonoBehaviour
{
    [SerializeField]
    private Vector3 axes;                           //Posibles ejes del escalado
    public float scaleUnits;                        //Velocidad del escalado


    // Update is called once per frame
    void Update()
    {
        //Acotacion de los valores de escalado al valor unitario [-1,1]
        axes = CapsuleMovement.ClampVector3(axes);

        //La escala, al contrario que la rotacion y el movimiento, es acumulativa
        //Lo que quiere decir que debemos a�adir el nuevo valor de la escala al valor anterior
        transform.localScale += axes * (scaleUnits * Time.deltaTime);

    }
}

