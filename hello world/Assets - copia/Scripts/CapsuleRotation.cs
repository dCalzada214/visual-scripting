using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
    public float RotationSpeed;

    public Vector3 RotationAxis;
   

    // Update is called once per frame
    void Update()
    {
        RotationAxis = CapsuleRotation.ClampVector(RotationAxis);

        transform.Rotate(RotationAxis * (RotationSpeed * Time.deltaTime));
    }

    public static Vector3 ClampVector(Vector3 target)
    {
        float clampedX = Mathf.Clamp(target.x, -90f, 90f);
        float clampedY = Mathf.Clamp(target.y, -90f, 90f);
        float clampedZ = Mathf.Clamp(target.z, -90f, 90f);
        Vector3 result = new Vector3(clampedX, clampedY, clampedZ);
        return result;
    }
}
